/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeinheritance;

/**
 *
 * @author patthamawan
 */
public class TestShape{

    public static void main(String[] args){
        Shape shape = new Shape();
        Circle circle = new Circle (3);
        System.out.println(circle.CalArea());
        Circle circle1 = new Circle(4);
        System.out.println(circle.CalArea());
        System.out.println("---------");
        Triangle triangle = new Triangle(3,4);
        System.out.println(triangle.CalArea());
        Triangle triangle1 = new Triangle(4,3);
        System.out.println(triangle.CalArea());
        System.out.println("---------");
        Rectangle rectangle = new Rectangle(4,3);
        System.out.println(rectangle.CalArea());
        System.out.println("---------");
        Square square = new Square(2);
        System.out.println(square.CalArea());
        System.out.println("---------");
        
        
        Shape[] shapes = {circle, triangle, rectangle, square};
        for(int i=0; i< shapes.length; i++){
            System.out.println(shapes[i].CalArea());
            System.out.println("---------");
        }
        
    }
    
}
