/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeinheritance;

/**
 *
 * @author patthamawan
 */
public class Rectangle extends Shape{
    private final double w;
    private final double h;
    public Rectangle(double w, double h){
        System.out.println("Rectangle Created");
        this.w =w;
        this.h =h;
        
    }
    
    public double CalArea(){
        return w*h;
        
    }
}
