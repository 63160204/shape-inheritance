/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeinheritance;

/**
 *
 * @author patthamawan
 */
public class Circle extends Shape{
    
    private final double r;
    public static final double pi = 22.0/7;
    
    public Circle(double r){
        System.out.println("Circle Created");
        this.r = r;
        
    }
    
    @Override
    public double CalArea(){
        return pi*r*r;
    }
}
